package fr.ppaquin.microservices.ean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import fr.ppaquin.microservices.dvdfr.DvdFrServiceApplication;
import fr.ppaquin.microservices.dvdfr.dto.Dvd;
import fr.ppaquin.microservices.dvdfr.service.DvdFrService;

@SpringBootTest(classes = DvdFrServiceApplication.class)
public class DvdFrServiceSpec {

	@Autowired DvdFrService dvdFrService;
	@Autowired private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;
	private List<Dvd> dvds;

	// @formatter:off
	private String dvdFrFound1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"
			+ "<dvds generator=\"$Id: dvd.tpl 855 2008-08-04 15:53:24Z glapierre $\">\r\n"
			+ "  <dvd>\r\n"
			+ "    <id>12996</id>\r\n"
			+ "        <media>DVD</media>\r\n"
			+ "    <cover>https://dvdfr.com/mycover.jpg</cover>\r\n"
			+ "    <titres>\r\n"
			+ "        <fr>title1</fr>\r\n"
			+ "        <vo></vo>\r\n"
			+ "        <alternatif>title2</alternatif>\r\n"
			+ "        <alternatif_vo></alternatif_vo>\r\n"
			+ "    </titres>\r\n"
			+ "    <annee>1979</annee>\r\n"
			+ "    <edition>Édition Simple</edition>\r\n"
			+ "    <editeur>20th Century Fox</editeur>\r\n"
			+ "    <stars>\r\n"
			+ "        <star type=\"Réalisateur\" id=\"2039\">Ridley Scott</star>\r\n"
			+ "    </stars>\r\n"
			+ "  </dvd>\r\n"
			+ "</dvds>";
	
	private String dvdNoAlternative = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
			+ "<dvds generator=\"$Id: dvd.tpl 855 2008-08-04 15:53:24Z glapierre $\">"
			+ "<dvd>"
			+ "<id>40269</id>"
			+ "<media>DVD</media>"
			+ "<cover>https://dvdfr.com/mycover.jpg</cover>"
			+ "<titres> <fr>Le Grand bleu</fr> <vo></vo> <alternatif></alternatif> <alternatif_vo></alternatif_vo> </titres>"
			+ "<annee>1988</annee>"
			+ "<edition>Édition spéciale - 20ème Anniversaire</edition>"
			+ "<editeur>Gaumont</editeur>"
			+ "<stars> <star type=\"Réalisateur\" id=\"5696\">Luc Besson</star> </stars>"
			+ "</dvd>"
			+ "</dvds>";
	// @formatter:on

	@BeforeEach
	void setup() {
		this.mockServer = MockRestServiceServer.createServer(restTemplate);
	}

	@Test
	void shouldReturnFetchedDvd() throws Exception {
		givenDvdFrApiReturning(dvdFrFound1);

		whenSearchingEan("3344428015602");

		thenExpectDvds(Arrays.asList(	new Dvd(12996, "3344428015602", "title1", 1979, "https://dvdfr.com/mycover.jpg"),
										new Dvd(12996, "3344428015602", "title2", 1979,
												"https://dvdfr.com/mycover.jpg")));
	}

	@Test
	void shouldIgnoreEmptyTitleFetchedDvd() throws Exception {
		givenDvdFrApiReturning(dvdNoAlternative);

		whenSearchingEan("3344428015602");

		thenExpectDvds(Arrays.asList(new Dvd(40269, "3344428015602", "Le Grand bleu", 1988,
				"https://dvdfr.com/mycover.jpg")));
	}

	private void givenDvdFrApiReturning(String wantedXml) throws URISyntaxException {
		mockServer	.expect(ExpectedCount.once(),
							requestTo(new URI("https://www.dvdfr.com/api/search.php?gencode=3344428015602")))
					.andExpect(method(HttpMethod.GET))
					.andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(wantedXml));
	}

	private void whenSearchingEan(String ean) throws ParserConfigurationException, SAXException, IOException {
		dvds = dvdFrService.find(ean);
	}

	private void thenExpectDvds(List<Dvd> expected) {
		this.mockServer.verify();
		assertThat(expected).isEqualTo(dvds);
	}

}
