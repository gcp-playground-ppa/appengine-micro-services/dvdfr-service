package fr.ppaquin.microservices.ean;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ppaquin.microservices.dvdfr.DvdFrServiceApplication;
import fr.ppaquin.microservices.dvdfr.dto.Dvd;
import fr.ppaquin.microservices.dvdfr.service.DvdFrService;
import fr.ppaquin.microservices.ean.utils.WithOAuthSubject;

@WebAppConfiguration
@SpringBootTest(classes = DvdFrServiceApplication.class)
public class DvdFrEndPointSpec {

	@Autowired
	private WebApplicationContext webApplicationContext;

	@MockBean
	private DvdFrService dvdFrService;

	protected MockMvc mockMvc;
	private MvcResult mvcResult;
	private ObjectMapper mapper = new ObjectMapper();

	@BeforeEach
	void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		this.mvcResult = null;
	}

	@Test
	@WithOAuthSubject(scopes = { "${dvdfr.authority.dvd}" })
	void shouldCallDvdFrService() throws Exception {
		givenDvdFrServiceReturning(asList(new Dvd("123")));

		whenGetServicePath("/?ean=123");

		thenExpectApiReturning(new Dvd[] { new Dvd("123") });
	}

	@Test
	@WithOAuthSubject(scopes = { "${dvdfr.authority.dvd}" })
	void shouldHandleServiceException() throws Exception {
		when(dvdFrService.find("123")).thenThrow(new SAXException("Some Error occured"));

		whenGetServicePath("/?ean=123");

		assertEquals(500, mvcResult.getResponse().getStatus());
		assertThat("Some Error occured").isEqualTo(mvcResult.getResponse().getContentAsString());
	}

	private void givenDvdFrServiceReturning(List<Dvd> wantedList) throws Exception {
		when(dvdFrService.find("123")).thenReturn(wantedList);
	}

	private void whenGetServicePath(String path) throws Exception {
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path);
		mvcResult = this.mockMvc.perform(requestBuilder).andReturn();
	}

	private void thenExpectApiReturning(Dvd[] expected) throws Exception {
		assertEquals(200, mvcResult.getResponse().getStatus());

		verify(dvdFrService, times(1)).find("123");

		String receivedJson = mvcResult.getResponse().getContentAsString();
		assertThat(expected).isEqualTo(mapper.readValue(receivedJson, Dvd[].class));
	}

}
