package fr.ppaquin.microservices.dvdfr;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import fr.ppaquin.microservices.dvdfr.dto.Dvd;
import fr.ppaquin.microservices.dvdfr.service.DvdFrService;

@RestController
@RequestMapping("/")
public class DvdFrEndPointController {

	Logger logger = LoggerFactory.getLogger(DvdFrEndPointController.class);

	@Autowired
	DvdFrService dvdFrService;

	@GetMapping
	public List<Dvd> getDvdsFromEan(@RequestParam(name = "ean") String ean)
			throws ParserConfigurationException, SAXException, IOException {

		Collection<? extends GrantedAuthority> authorities = SecurityContextHolder	.getContext()
																					.getAuthentication()
																					.getAuthorities();
		return dvdFrService.find(ean);
	}

	@ExceptionHandler({ SAXException.class })
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleSaxException(SAXException exception) {

		return exception.getMessage();
	}

}
