package fr.ppaquin.microservices.dvdfr.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Dvd {
	private int dvdfr;
	private String ean;
	private String title;
	private int year;
	private String cover;

	public Dvd(String ean) {
		this.ean = ean;
	}

	public int getDvdfr() {
		return dvdfr;
	}

	public String getEan() {
		return ean;
	}

	public String getTitle() {
		return title;
	}

	public int getYear() {
		return year;
	}

	public String getCover() {
		return cover;
	}

}
