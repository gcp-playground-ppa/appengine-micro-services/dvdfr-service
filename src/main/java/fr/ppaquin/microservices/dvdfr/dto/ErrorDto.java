package fr.ppaquin.microservices.dvdfr.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class ErrorDto {

	private int status;
	private String message;

	public ErrorDto(int status, String message) {
		this.status = status;
		this.message = message;
	}

}
