package fr.ppaquin.microservices.dvdfr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DvdFrServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DvdFrServiceApplication.class, args);
	}

}
