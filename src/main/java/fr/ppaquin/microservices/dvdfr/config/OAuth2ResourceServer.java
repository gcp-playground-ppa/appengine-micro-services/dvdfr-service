package fr.ppaquin.microservices.dvdfr.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;

@Configuration
@EnableWebSecurity
public class OAuth2ResourceServer extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {// @formatter:off
		http
        .httpBasic().disable()  
        .formLogin(AbstractHttpConfigurer::disable)  
        .csrf(AbstractHttpConfigurer::disable)  
		.authorizeRequests(authorize -> authorize  
				.mvcMatchers("/actuator/health").permitAll()
	            .mvcMatchers(HttpMethod.GET, "/**").hasAuthority("SCOPE_write:dvd")  
	            .mvcMatchers(HttpMethod.POST, "/**").hasAuthority("SCOPE_write:dvd")  
	            .anyRequest().authenticated()  
	        )
        .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)  ;
	}
	// @formatter:on
}
