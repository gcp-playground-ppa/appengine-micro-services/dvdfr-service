package fr.ppaquin.microservices.dvdfr.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import fr.ppaquin.microservices.dvdfr.dto.Dvd;
import fr.ppaquin.microservices.dvdfr.handler.MatchingHandler;

@Service
public class DvdFrService {

	@Autowired private RestTemplate restTemplate;

	private final String url = "https://www.dvdfr.com/api/search.php?gencode=";

	public List<Dvd> find(String ean) throws ParserConfigurationException, SAXException, IOException {

		String fullUrl = url + ean;
		ResponseEntity<String> response = restTemplate.getForEntity(fullUrl, String.class);
		String xmlString = response.getBody();

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		MatchingHandler matchingHandler = new MatchingHandler();
		saxParser.parse(IOUtils.toInputStream(xmlString, Charset.defaultCharset()), matchingHandler);

		return matchingHandler	.getTitles()
								.stream()
								.map(title -> new Dvd(matchingHandler.getId(), ean, title, matchingHandler.getYear(),
										matchingHandler.getCover()))
								.collect(Collectors.toList());
	}
}
