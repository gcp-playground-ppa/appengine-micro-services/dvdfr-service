package fr.ppaquin.microservices.dvdfr.handler;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.util.StringUtils;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MatchingHandler extends DefaultHandler {

	Collection<String> titles = new ArrayList<>();
	int year = -1;
	int id = -1;
	String cover = "";

	private String elementValue = "";

	public Collection<String> getTitles() {
		return titles;
	}

	public int getId() {
		return id;
	}

	public int getYear() {
		return year;
	}

	public String getCover() {
		return cover;
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		elementValue = new String(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
		case "fr":
		case "alternatif":
			if (!StringUtils.isEmpty(elementValue.trim())) {
				titles.add(elementValue);
			}
			break;
		case "annee":
			year = Integer.parseInt(elementValue);
			break;
		case "id":
			id = Integer.parseInt(elementValue);
			break;
		case "cover":
			cover = elementValue;
			break;
		}
	}

}
