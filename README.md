API utilisant l'API DVDFR https://www.dvdfr.com/api/ 

# Use Locally
1. Compile `mvn clean package`
2. Start server `java -jar target\dvdfr-service-0.0.1-SNAPSHOT.jar`
3. This microservice is deployed with default profile "dev" to localhost port 9002, it's an authenticated API using OAuth2, Bearer token is mandatory.

Exemples de code barre : 
- 3607483160671
- 7321950586097
- 3333297192910
- 3344428015602
- 3459370501285

# Deploy to Google Cloud App Engine

1.  Compile : `mvn clean package -Pgcp`
2.  Connect to your GCP account, verify that API Cloud Compile is enabled.
4.  Deploy to GCP : `mvn appengine:deploy`

# Eléménts de context : 
- DVDFR est une source simple d'informations à partir du code barre du DVD.
- Google Cloud Appengine propose une solution basé sur le DNS pour gérer le load balancing et la montée en charge (création d'instances du service).
- Eureka et Ribbon sont inutiles car remplacés par le service DNS et les instances de service Google Cloud AppEngine.

# TODO : 
- Migration vers une solution serverless (Google Cloud Functions)
